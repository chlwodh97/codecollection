package com.chlwodh97.codecollection;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CodeCollectionApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodeCollectionApplication.class, args);
	}

}
