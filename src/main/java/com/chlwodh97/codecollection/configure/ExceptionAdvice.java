package com.chlwodh97.codecollection.configure;

import com.chlwodh97.codecollection.enums.ResultCode;
import com.chlwodh97.codecollection.exception.CBusinessJoinOverlapException;
import com.chlwodh97.codecollection.model.common.CommonResult;
import com.chlwodh97.codecollection.service.ResponseService;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionAdvice {
    /**
     * 기본 실패 메세지
     */
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, Exception e) {
        return ResponseService.getFailResult(ResultCode.FAILURE);
    }
//    /**
//     * 비밀번호 확인 실패 메세지
//     */
//    @ExceptionHandler(CMemberPasswordException.class)
//    @ResponseStatus(HttpStatus.BAD_REQUEST)
//    protected CommonResult memberPasswordException(HttpServletRequest request, CMemberPasswordException e) {
//        return ResponseService.getFailResult(ResultCode.PASSWORD);
//    }
    /**
     *  중복 불가 메세지 사업장 등록 요청
     */
    @ExceptionHandler(CBusinessJoinOverlapException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult businessJoinOverlapException(HttpServletRequest request, Exception e) {
        return ResponseService.getFailResult(ResultCode.BUSINESS_OVERLAP);
    }
//    /**
//     *  게시글 신고 중복 불가 메세지
//     */
//    @ExceptionHandler(CReportBoardOverlapException.class)
//    @ResponseStatus(HttpStatus.BAD_REQUEST)
//    protected CommonResult reportBoardOverlapException(HttpServletRequest request, Exception e){
//        return ResponseService.getFailResult(ResultCode.REPORT_BOARD_OVERLAP);
//    }
//
//    /**
//     * 댓글 신고 중복 불가 메세지
//     */
//    @ExceptionHandler(CReportCommentOverlapException.class)
//    @ResponseStatus(HttpStatus.BAD_REQUEST)
//    protected CommonResult reportCommentOverlapException(HttpServletRequest request, Exception e){
//        return ResponseService.getFailResult(ResultCode.REPORT_COMMENT_OVERLAP);
//    }

}
