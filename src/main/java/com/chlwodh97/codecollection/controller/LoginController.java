package com.chlwodh97.codecollection.controller;


import com.chlwodh97.codecollection.model.member.LoginRequest;
import com.chlwodh97.codecollection.model.member.LoginResponse;
import com.chlwodh97.codecollection.model.common.SingleResult;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//@RestController
//@RequiredArgsConstructor
//@RequestMapping("/v1/login")
//public class LoginController {
//    private final LoginService loginService;
//
//
//    @PostMapping("/member")
//    @Operation(summary = "로그인")
//    public SingleResult<LoginResponse> doLogin(@RequestBody LoginRequest request){
//        return loginService.doLogin(request);
//    }
//}
