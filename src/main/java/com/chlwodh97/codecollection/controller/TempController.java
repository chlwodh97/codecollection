package com.chlwodh97.codecollection.controller;

import com.chlwodh97.codecollection.model.common.CommonResult;
import com.chlwodh97.codecollection.service.ResponseService;
import com.chlwodh97.codecollection.service.TempService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/temp")
public class TempController {
    private final TempService tempService;

    @PostMapping("/file-upload")
    @Operation(summary = "데이터 베이스 유저 정보 파일 업로드")
    public CommonResult setMemberByFile(@RequestParam("csvFile")MultipartFile csvFile) throws IOException {
        tempService.setMemberByFile(csvFile);

        return ResponseService.getSuccessResult();
    }
}
