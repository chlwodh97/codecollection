package com.chlwodh97.codecollection.entity;


import com.chlwodh97.codecollection.interfaces.CommonModelBuilder;
import com.chlwodh97.codecollection.model.img.ImgRequest;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Image {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String img;

    private Image (Bulder bulder) {
        this.img = bulder.img;
    }

    public static class Bulder implements CommonModelBuilder<Image> {
        private final String img;

        public Bulder(ImgRequest request){
            this.img = request.getImg();
        }
        @Override
        public Image build() {
            return new Image(this);
        }
    }
}
