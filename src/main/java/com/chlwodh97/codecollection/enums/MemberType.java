package com.chlwodh97.codecollection.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum MemberType {
    GENERAL("일반회원"),
    BOSS("사업자"),
    MANAGEMENT("관리자");

    private final String name;
}
