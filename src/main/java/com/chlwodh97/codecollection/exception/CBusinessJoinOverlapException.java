package com.chlwodh97.codecollection.exception;

public class CBusinessJoinOverlapException extends RuntimeException{

    public CBusinessJoinOverlapException(String msg, Throwable t) {
        super(msg,t);
    }
    public CBusinessJoinOverlapException(String msg) {
        super(msg);
    }
    public CBusinessJoinOverlapException() {
        super();
    }
}