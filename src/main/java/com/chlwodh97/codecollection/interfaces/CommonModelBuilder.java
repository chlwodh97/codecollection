package com.chlwodh97.codecollection.interfaces;


/**
 * 빌드용
 */
public interface CommonModelBuilder<T> {
    T build();
}

