package com.chlwodh97.codecollection.lib;

public class CommonCheck {
    public static boolean checkUsername(String username) {
        // 사용자 입력에 대한 검증
        String pattern = "^[a-zA-z]{1}[a-zA-z0-9]{4,19}$";
        return username.matches(pattern);
    }
}
