package com.chlwodh97.codecollection.model.img;


import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
public class ImgRequest {
    private String img;
}
