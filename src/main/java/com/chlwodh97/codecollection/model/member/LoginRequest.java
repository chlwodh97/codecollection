package com.chlwodh97.codecollection.model.member;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginRequest {
    private String homeId;
    private String Password;
}
