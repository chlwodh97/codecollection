package com.chlwodh97.codecollection.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginResponse {

    private Long userId;
    private String nickName;
}
