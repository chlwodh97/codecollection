package com.chlwodh97.codecollection.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberChangeRequest {
    private String name;
    private String phoneNumber;
    private String address;
}
