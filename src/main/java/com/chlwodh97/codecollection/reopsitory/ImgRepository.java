package com.chlwodh97.codecollection.reopsitory;

import com.chlwodh97.codecollection.entity.Image;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImgRepository extends JpaRepository<Image, Long> {
}
