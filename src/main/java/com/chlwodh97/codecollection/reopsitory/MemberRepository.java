package com.chlwodh97.codecollection.reopsitory;

import com.chlwodh97.codecollection.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Long> {
}
