package com.chlwodh97.codecollection.service;


import com.chlwodh97.codecollection.lib.CommonFile;
import com.chlwodh97.codecollection.reopsitory.ImgRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

@Service
@RequiredArgsConstructor
public class ImgService {
    private final ImgRepository imgRepository;


    public void setImgFile(MultipartFile multipartFile) throws IOException {
        File file = CommonFile.mltipartToFile(multipartFile);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));


        bufferedReader.close();

    }
}
