package com.chlwodh97.codecollection.service;

import com.chlwodh97.codecollection.entity.Member;
import com.chlwodh97.codecollection.lib.CommonFile;
import com.chlwodh97.codecollection.reopsitory.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
@Service
@RequiredArgsConstructor
public class TempService {
    private final MemberRepository memberRepository;

    /**
     *
     * @param multipartFile 데이터 베이스에 회원 정보
     * @throws IOException 한번에 업로드
     */
    public void setMemberByFile(MultipartFile multipartFile) throws IOException {
        File file = CommonFile.mltipartToFile(multipartFile);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

        //버퍼에서 넘어온 String 한 줄을 임시로 담아 둘 변수 가 필요함
        String line = "";

        // 버퍼에선 번호를 줄 필요가 없다
        // 줄 번호 수동 체크를 위해 int 로 줄 번호 하나씩 1씩 증가 해서 기록 할 변수 필요함
        int index = 0;

        while ((line = bufferedReader.readLine()) != null) {
            if (index > 0) {
                String[] cols = line.split(",");
                if (cols.length == 9) {
                    memberRepository.save(new Member.BuilderCsv(cols).build());
                }
            }
            index++;
            // 자기 자신에 ++
            // index는 몇번 째 줄인지 표시 + 0번째 데이터 안보이기 -> 맨위의 설명란은 필요없으니
        }
        bufferedReader.close();
    }
}
